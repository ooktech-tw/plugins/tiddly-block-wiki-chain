/*\
title: $:/plugins/OokTech/TiddlyBlock/ActionWikiChain.js
type: application/javascript
module-type: widget

This is an action widget to interface with the TiddlyBlock WikiChain
\*/
(function () {

  /*jslint node: true, browser: true */
  /*global $tw: false */
  "use strict";

  const Widget = require("$:/core/modules/widgets/widget.js").widget;

  const WikiChain = function(parseTreeNode,options) {
    this.initialise(parseTreeNode,options);
  };

  /*
  Inherit from the base widget class
  */
  WikiChain.prototype = new Widget();

  /*
  Render this widget into the DOM
  */
  WikiChain.prototype.render = function(parent,nextSibling) {
    this.computeAttributes();
    this.execute();
  };


  /*
  Compute the internal state of the widget
  */
  WikiChain.prototype.execute = function() {
    this.tiddler = this.getAttribute('tiddler', undefined)
    this.newblock = this.getAttribute('newblock', false)
    this.utilicoin = this.getAttribute('utilicoin', 0)
    this.sender = this.getAttribute('sender', '')
    this.recipient = this.getAttribute('recipient', '')
  };

  /*
  Refresh the widget by ensuring our attributes are up to date
  */
  WikiChain.prototype.refresh = function(changedTiddlers) {
    const changedAttributes = this.computeAttributes();
    if(Object.keys(changedAttributes).length) {
      this.refreshSelf();
      return true;
    }
    return this.refreshChildren(changedTiddlers);
  };

  /*
  Invoke the action associated with this widget
  */
  WikiChain.prototype.invokeAction = function(triggeringWidget,event) {
    if(this.tiddler || this.utilicoin) {
      this.wiki.WikiChain.new_transaction(this.sender, this.recipient, this.utilicoin, $tw.wiki.getTiddler(this.tiddler))
      const fields = {
        title: '$:/data/TiddlyBlock/WikiChain/pending',
        text: JSON.stringify($tw.wiki.WikiChain.current_transactions),
        type: 'application/javascript'
      }
      $tw.wiki.addTiddler(new $tw.Tiddler(fields));
    }
    if(this.newblock) {
        this.wiki.WikiChain.mint(function() {
          let fields = {
            title: '$:/data/TiddlyBlock/WikiChain',
            text: JSON.stringify($tw.wiki.WikiChain._chain),
            type: 'application/javascript'
          }
          $tw.wiki.addTiddler(new $tw.Tiddler(fields));
          fields = {
            title: '$:/data/TiddlyBlock/WikiChain/pending',
            text: JSON.stringify($tw.wiki.WikiChain.current_transactions),
            type: 'application/javascript'
          }
          $tw.wiki.addTiddler(new $tw.Tiddler(fields));
        })
    }
  };

  exports['action-wikichain'] = WikiChain
})();
