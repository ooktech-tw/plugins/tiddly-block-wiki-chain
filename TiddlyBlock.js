/*\
title: $:/plugins/OokTech/TiddlyBlock/TiddlyBlock.js
type: application/javascript
module-type: startup

The TiddlyBlock WikiChain

A blockchain and cryptocurrency for tiddlywiki

\*/
(function(){

/*jslint node: true, browser: true */
/*global $tw: false */
"use strict";

// Export name and synchronous status
exports.name = "tiddlyblock";
exports.platforms = ["browser"];
exports.after = ["render"];
exports.synchronous = true;

// Utilicoin
// utile
class TiddlerBlock {
  constructor(index, timestamp, transactions, proof, previous_hash) {
    this._index = index
    this._timestamp = timestamp
    this._transactions = transactions
    this._proof = proof
    this._previous_hash = previous_hash
  }
  get timestamp() {
    return this._timestamp;
  }
  get transactions() {
    return this._transactions;
  }
  get proof() {
    return this._proof;
  }
  get previous_hash() {
    return this._previous_hash;
  }
  get index() {
    return this._index;
  }
  get payload() {
    return this._payload;
  }
}

class TiddlyChain {
  constructor() {
    this._chain =  [];
    this._nodes = new Set();
    this._current_transactions = [];
    //Add genesis block
    this.new_block(100,1);
  }
  get chain() {
    return this._chain;
  }
  get nodes() {
    return this._nodes;
  }
  set current_transactions(transactions) {
    this._current_transactions = transactions;
  }
  get current_transactions() {
    return this._current_transactions
  }
  mint(cb) {
    if(this.current_transactions) {
      const that = this
      const prev = this.last_block().previous_hash
      console.log('previous hash:', prev)
      this.proof_of_work(prev).then(function(theProof) {
        console.log(theProof)
        that.new_block(theProof,null,cb)
      })
    } else {
      console.log('no new transactions!')
    }
  }
  new_block(proof,previous_hash = null,cb=undefined) {
    var that = this;
    var previous_index;
    let time = new Date();
    if (this.chain.length == 0) {
      previous_index = 0;
    } else {
      previous_index = this.chain.length -1 ;
    }
    if(previous_hash) {
      let block = new TiddlerBlock(that.chain.length+1, time, that.current_transactions, proof, previous_hash);
      //Reset the current list of transactions
      this.current_transactions = [];
      this.chain.push(block);
      if(typeof cb === 'function') {
        cb()
      }
    } else {
      return this.hash(this.chain[previous_index]).then(function(the_hash) {
        let block = new TiddlerBlock(that.chain.length+1, time, that.current_transactions, proof, the_hash);
        //Reset the current list of transactions
        that.current_transactions = [];
        that.chain.push(block);
        if(typeof cb === 'function') {
          cb()
        }
      })
    }
  }
  last_block() {
    if (this.chain.length == 0) {
      return 0;
    }
    return this.chain[this.chain.length -1];
  }
  new_transaction(sender,recipient,amount,payload) {
    this.current_transactions.push({
      sender: sender,
      utilicoin: amount,
      recipient : recipient,
      tiddlers: payload
    });
    //return the index of the next block to be mined.
    if (this.chain.length == 0) {
      return 1;
    } else {
      return this.chain[this.chain.length -1].index +1;
    }
  }
  proof_of_work(last_proof, start=0) {
    let that = this
    const te = new TextEncoder()
    let block_string = te.encode(last_proof)
    block_string =  uint8concat([block_string, te.encode(String(start))])
    return crypto.subtle.digest('SHA-256', block_string).then(function(hashBuffer) {
      const hashArray = Array.from(new Uint8Array(hashBuffer));
      const hashHex = hashArray.map(b => b.toString(16).padStart(2, '0')).join('');
      if(hashHex.startsWith('002')) {
        return new Promise(function(resolve, reject) {
          resolve(start)
        })
      } else {
        return that.proof_of_work(last_proof, start+1)
      }
    })
  }
  valid_proof(last_proof,proof) {
    let that = this
    const te = new TextEncoder()
    let block_string = te.encode(last_proof)
    block_string =  uint8concat([block_string, te.encode(String(start))])
    return crypto.subtle.digest('SHA-256', block_string).then(function(hashBuffer) {
      const hashArray = Array.from(new Uint8Array(hashBuffer));
      const hashHex = hashArray.map(b => b.toString(16).padStart(2, '0')).join('');
      if(hashHex.startsWith('002')) {
        return true
      } else {
        return false
      }
    })


    let guess = Buffer.from(proof.toString()).toString('base64') + Buffer.from(last_proof.toString()).toString('base64');
    var hash = createHash.createHash('sha256')
      .update(guess)
      .digest('base64');
    return hash.startsWith('002');
  }
  register_node(address) {
    var parsed_url = new URL(address); // URL documentation can be found at https://nodejs.org/api/url.html#url_the_whatwg_url_api
    this._nodes.add(parsed_url);
  }
  valid_chain(chain_to_check, ind) {
    if(length(chain_to_check) === 0 || length(chain_to_check) < ind+1) {
      return new Promise(function(reslove,reject){
        resolve(true)
      })
    }
    const last_block = chain_to_check[ind]
    const block = chain_to_check[ind+1]
    const that = this
    this.valid_proof(last_block.proof, block.proof).then(function(result) {
      if(result) {
        return that.valid_chain(chain_to_check, ind+1)
      } else {
        return false
      }
    })
  }
  resolve_conflicts() {
    const neighbours = this.nodes;
    const that = this;
    var new_chain = null;
    var flag = 0;
    var tmp_length = 0;
    max_length = this.chain.length;
    neighbours.forEach(node => {
      var xmlHttp = new XMLHttpRequest();
      xmlHttp.onreadystatechange = function() {
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
          var new_chain = JSON.parse(xmlHttp.responseText);
          if (that.valid_chain(new_chain, 0)  && new_chain.length > max_length) {
            that._chain = new_chain;
            max_length = new_chain.length;
          }
        }
      };
      xmlHttp.open("GET", node.origin + "/chain", true);
      xmlHttp.send(null);
    });
  }
  hash(block) {
    let block_string = new TextEncoder().encode(JSON.stringify(block))
    return crypto.subtle.digest('SHA-256', block_string).then(function(hashBuffer) {
      const hashArray = Array.from(new Uint8Array(hashBuffer));
      const hashHex = hashArray.map(b => b.toString(16).padStart(2, '0')).join('');
      return hashHex;
    })
  }
}

function uint8concat(arrays) {
  const totalLength = arrays.reduce((acc, value) => acc + value.length, 0);
  if (!arrays.length) return null;
  let result = new Uint8Array(totalLength);
  // for each array - copy it over result
  // next array is copied right after the previous one
  let length = 0;
  for(let array of arrays) {
    result.set(array, length);
    length += array.length;
  }
  return result;
}

$tw.wiki.WikiChain = new TiddlyChain()

})();